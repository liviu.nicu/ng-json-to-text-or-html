export declare class cstJsonTransformer {
    private finalText;
    private finalHTML;
    private condition;
    private templateTypes;
    private addTitle;
    private init;
    private addSubtitle;
    private addParagraph;
    private addList;
    toText: (obj: any) => any;
    toHTML: (obj: any) => any;
}
