(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common'], factory) :
    (global = global || self, factory((global['cst-json-transformer'] = global['cst-json-transformer'] || {}, global['cst-json-transformer'].umd = global['cst-json-transformer'].umd || {}, global['cst-json-transformer'].umd.js = {}), global.core, global.common));
}(this, function (exports, core, common) { 'use strict';

    var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var cstJsonTransformer = /** @class */ (function () {
        function cstJsonTransformer() {
            this.finalText = '';
            this.finalHTML = '';
            this.condition = {
                text: 'text',
                HTML: 'HTML'
            };
            this.templateTypes = {
                title: "title",
                subtitle: "subtitle",
                list: "list",
                paragraph: "paragraph"
            };
            this.addTitle = function (text, condition) {
                if (condition == this.condition.text) {
                    return text + "\n\n";
                }
                if (condition == this.condition.HTML) {
                    return "<h2>" + text + "</h2>";
                }
            };
            this.init = function () {
                this.finalText = '';
                this.finalHTML = '';
            };
            this.addSubtitle = function (text, condition) {
                if (condition == this.condition.text) {
                    return text + "\n\n";
                }
                if (condition == this.condition.HTML) {
                    return "<h4>" + text + "</h4>";
                }
            };
            this.addParagraph = function (text, condition) {
                if (condition == this.condition.text) {
                    return text + "\n\n";
                }
                if (condition == this.condition.HTML) {
                    return "<p>" + text + "</p>";
                }
            };
            this.addList = function (list, condition) {
                if (condition == this.condition.text) {
                    var generatedText_1 = "";
                    list.map(function (currentItem) { generatedText_1 += "\t" + decodeURIComponent("%E2%80%A2") + " " + currentItem + "\n"; });
                    return generatedText_1;
                }
                if (condition == this.condition.HTML) {
                    var generatedHtml_1 = "";
                    list.map(function (currentItem) { generatedHtml_1 += "<li>" + currentItem + "</li>"; });
                    return "<ul>" + generatedHtml_1 + "</ul>";
                }
            };
            this.toText = function (obj) {
                var _this = this;
                this.init();
                if (Array.isArray(obj)) {
                    obj.map(function (currentItem, index) {
                        if (currentItem.type == _this.templateTypes.title) {
                            _this.finalText += _this.addTitle(currentItem.text ? currentItem.text : '', _this.condition.text);
                        }
                        if (currentItem.type == _this.templateTypes.subtitle) {
                            _this.finalText += _this.addSubtitle(currentItem.text ? currentItem.text : '', _this.condition.text);
                        }
                        if (currentItem.type == _this.templateTypes.list) {
                            _this.finalText += _this.addList(currentItem.items ? currentItem.items : [], _this.condition.text);
                        }
                        if (currentItem.type == _this.templateTypes.paragraph) {
                            _this.finalText += _this.addParagraph(currentItem.text ? currentItem.text : [], _this.condition.text);
                        }
                    });
                }
                else {
                    console.error("this is not an array");
                }
                return this.finalText;
            };
            this.toHTML = function (obj) {
                var _this = this;
                this.init();
                if (Array.isArray(obj)) {
                    obj.map(function (currentItem, index) {
                        if (currentItem.type == _this.templateTypes.title) {
                            _this.finalHTML += _this.addTitle(currentItem.text ? currentItem.text : '', _this.condition.HTML);
                        }
                        if (currentItem.type == _this.templateTypes.subtitle) {
                            _this.finalHTML += _this.addSubtitle(currentItem.text ? currentItem.text : '', _this.condition.HTML);
                        }
                        if (currentItem.type == _this.templateTypes.list) {
                            _this.finalHTML += _this.addList(currentItem.items ? currentItem.items : [], _this.condition.HTML);
                        }
                        if (currentItem.type == _this.templateTypes.paragraph) {
                            _this.finalHTML += _this.addParagraph(currentItem.text ? currentItem.text : [], _this.condition.HTML);
                        }
                    });
                }
                else {
                    console.error("this is not an array");
                }
                return this.finalHTML;
            };
        }
        cstJsonTransformer = __decorate([
            core.NgModule({
                imports: [common.CommonModule]
            })
        ], cstJsonTransformer);
        return cstJsonTransformer;
    }());

    exports.cstJsonTransformer = cstJsonTransformer;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
