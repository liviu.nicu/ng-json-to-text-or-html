export default {
    input: 'dist/index.js',
    output: {
        file:'dist/bundle/cst-json-transformer.umd.js',
        format: 'umd',
        name:'cst-json-transformer.umd.js'
      }
  }