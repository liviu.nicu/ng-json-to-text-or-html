# JSON TO TEXT OR HTML
Angular module for transforming JSON to plain text or HTML
## Installation
`
npm install cst-json-transformer
`
#### Add to app.module.ts

````
import {cstJsonTransformer} from 'cst-json-transformer/dist';
@NgModule({
  declarations: [],
  imports: [
    cstJsonTransformer
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
````

#### In component
````
import { Component, OnInit } from '@angular/core';
import {cstJsonTransformer} from 'cst-json-transformer/dist';
@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponent implements OnInit {

  constructor(private convertor: cstJsonTransformer) { }

  ngOnInit() {
    let model=[
      {
        type:'title',
        text:'Title text',
      },
      {
        type:'subtitle',
        text:'Subtitle text',
      },
      {
        type:'paragraph',
        text:'Text in paragraph',
      },
      {
        type:'list',
        items:["item 1","item 2"]
      }
    ]
    console.log("text: ", this.convertor.toText(model));
    console.log("HTML: ", this.convertor.toHTML(model));
  }
}
````




### Data model 
`
Data model should be an ARRAY of json objects with the following keys
`

| Key | Value |
| ------ | ------ |
| type | 'title','subtitle','paragraph' or 'list'|


If type == 'list'

| Key | Value |
| ------ | ------ |
| items | array of strings |



If type != 'list'


| Key | Value |
| ------ | ------ |
| text | The actual text that will be displayed in every type of template |



### Development

Want to contribute? Great! 
[GIT REPOSITORY](https://gitlab.com/liviu.nicu/ng-json-to-text-or-html)

