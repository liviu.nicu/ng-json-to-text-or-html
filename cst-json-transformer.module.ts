

import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [CommonModule]
  })

  
  export class cstJsonTransformer{

    private finalText:string='';
    private finalHTML:string='';
    private condition:any={
        text:'text',
        HTML:'HTML'
    }
    private templateTypes:any={
        title:"title",
        subtitle:"subtitle",
        list:"list",
        paragraph:"paragraph"
    }

    private addTitle=function(text:string,condition:string){
        if(condition==this.condition.text){
            return text+"\n\n";
        }

        if(condition==this.condition.HTML){
            return "<h2>"+text+"</h2>";
        }
    }

    private init=function(){
        this.finalText='';
        this.finalHTML='';
    }

    private addSubtitle=function(text:string,condition:string){
        if(condition==this.condition.text){
            return text+"\n\n";
        }

        if(condition==this.condition.HTML){
            return "<h4>"+text+"</h4>";
        }
    }

    private addParagraph=function(text:string,condition:string){
        if(condition==this.condition.text){
            return text+"\n\n";
        }

        if(condition==this.condition.HTML){
            return "<p>"+text+"</p>";
        }
    }
    private addList=function(list:any,condition:string){   
        if(condition==this.condition.text){
            let generatedText="";
            list.map((currentItem:string)=>{generatedText+="\t"+decodeURIComponent("%E2%80%A2")+" "+currentItem+"\n"});
            return generatedText
        }

        if(condition==this.condition.HTML){
            let generatedHtml="";
            list.map((currentItem:string)=>{generatedHtml+="<li>"+currentItem+"</li>"});
            return "<ul>"+generatedHtml+"</ul>";
        }
    }


    public toText=function(obj:any){
        this.init();
  
        if(Array.isArray(obj)){
            obj.map((currentItem,index)=>{
                if(currentItem.type==this.templateTypes.title){
                    this.finalText+=this.addTitle(currentItem.text?currentItem.text:'',this.condition.text)
                }
                if(currentItem.type==this.templateTypes.subtitle){
                    this.finalText+=this.addSubtitle(currentItem.text?currentItem.text:'',this.condition.text)
                }
                if(currentItem.type==this.templateTypes.list){
                    this.finalText+=this.addList(currentItem.items?currentItem.items:[],this.condition.text)
                }
                if(currentItem.type==this.templateTypes.paragraph){
                    this.finalText+=this.addParagraph(currentItem.text?currentItem.text:[],this.condition.text)
                }
            })
    }else{
        console.error("this is not an array")
    }

    return this.finalText;
    }

    public toHTML=function(obj:any){
        this.init();
        if(Array.isArray(obj)){
            obj.map((currentItem,index)=>{
                if(currentItem.type==this.templateTypes.title){
                    this.finalHTML+=this.addTitle(currentItem.text?currentItem.text:'',this.condition.HTML)
                }
                if(currentItem.type==this.templateTypes.subtitle){
                    this.finalHTML+=this.addSubtitle(currentItem.text?currentItem.text:'',this.condition.HTML)
                }
                if(currentItem.type==this.templateTypes.list){
                    this.finalHTML+=this.addList(currentItem.items?currentItem.items:[],this.condition.HTML)
                }
                if(currentItem.type==this.templateTypes.paragraph){
                    this.finalHTML+=this.addParagraph(currentItem.text?currentItem.text:[],this.condition.HTML)
                }
            })
        }else{
            console.error("this is not an array")
        }

        return this.finalHTML;
    }
}

